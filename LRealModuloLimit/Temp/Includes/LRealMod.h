/* Automation Studio generated header file */
/* Do not edit ! */
/* LRealMod  */

#ifndef _LREALMOD_
#define _LREALMOD_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif



/* Prototyping of functions and function blocks */
_BUR_PUBLIC double LRealModulusLimit(double UpperLimit, double value);


#ifdef __cplusplus
};
#endif
#endif /* _LREALMOD_ */

