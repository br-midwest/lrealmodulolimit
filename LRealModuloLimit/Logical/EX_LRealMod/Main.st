
PROGRAM _CYCLIC
	
	//	This function takes in the upper limit in the first param with the value in the second returning
	//	the corresponding number between limitNum and 0
	result := LRealModulusLimit(limitNum, num);
	
END_PROGRAM

