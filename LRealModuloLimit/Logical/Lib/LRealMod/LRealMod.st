
FUNCTION LRealModulusLimit
	
	
	//	Sets default return value
	LRealModulusLimit := value;
	holder := UpperLimit;
	
	
	//	Handles number above the limit
	IF (value > UpperLimit) THEN
		
		WHILE ((value - holder) > UpperLimit) DO
			holder := holder + UpperLimit;
		END_WHILE;
		
		LRealModulusLimit := value - holder;
		
	END_IF
	
	
	//	Handles negative number
	IF value < 0 THEN
		WHILE value < 0 DO
			value := value + UpperLimit;
		END_WHILE;
		LRealModulusLimit := value;
	END_IF
	
END_FUNCTION
